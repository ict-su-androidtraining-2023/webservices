import { IsNotEmpty, IsString, MinLength, minLength } from "class-validator";

export class CreateUserDto {
    @IsString()
    @MinLength(3, {message: "ชื่อน่าจะต้องมากกว่า 3 ตัวอักษรนะ"})
    @IsNotEmpty()
    firstname: string;

    @IsString()
    @IsNotEmpty()
    @MinLength(8, {message: 'StudentId น่าจะต้องมากกว่า 8 ตัวนะ'})
    studentId: string;

    profilePicture: string;

    @IsString()
    @IsNotEmpty()
    @MinLength(10, {message: 'ต้องส่ง Token มาด้วยนะ'})
    tokenNotification: string;
}
