import { Injectable } from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { User } from './entities/user.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class UserService {

  constructor(
    @InjectRepository(User) private readonly userRepo: Repository<User>,
  ) { }

  create(createUserDto: CreateUserDto) {
    const user: User = new User();
    user.firstname = createUserDto.firstname;
    user.studentId = createUserDto.studentId;
    user.profilePicture = createUserDto.profilePicture;
    user.tokenNotification = createUserDto.tokenNotification;
    return this.userRepo.save(user);
  }

  findAll() {
    return this.userRepo.find();
  }

  findOne(id: number) {
    return this.userRepo.findOneBy({ id });
  }

  findOneByUser(studentId: string) {
    return this.userRepo.createQueryBuilder('users')
    .where("users.student_id = :id", { id: studentId })
    .getOne();
  }

  findByToken(token: string) {
    return this.userRepo.createQueryBuilder('users')
    .where("users.token_notification = :token", { token: token })
    .getMany();
  }

  update(id: number, updateUserDto: UpdateUserDto) {
    const user: User = new User();
    user.firstname = updateUserDto.firstname;
    user.studentId = updateUserDto.studentId;
    user.profilePicture = updateUserDto.profilePicture;
    user.tokenNotification = updateUserDto.tokenNotification;
    user.id = id;

    return this.userRepo.save(user);
  }

  remove(id: number) {
    return this.userRepo.delete(id);
  }
}
