import { Controller, Get, Post, Body, Patch, Param, UseInterceptors, UploadedFile, HttpException, HttpStatus } from '@nestjs/common';
import { UserService } from './user.service';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { FileInterceptor, MulterModule } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import { User } from './entities/user.entity';

@Controller('user')
export class UserController {
  constructor(private readonly userService: UserService) { }

  @Post()
  async create(@Body() createUserDto: CreateUserDto) {

    let findDuplicate = await this.userService.findOneByUser(createUserDto.studentId);

    if (findDuplicate != null) {
      var id = findDuplicate.id;
      var updateUserDto = new UpdateUserDto();
      updateUserDto.firstname = createUserDto.firstname;
      updateUserDto.profilePicture = findDuplicate.profilePicture;
      updateUserDto.studentId = createUserDto.studentId;
      updateUserDto.tokenNotification = createUserDto.tokenNotification;
      var person = this.userService.update(+id, updateUserDto);
      return person;
    } else {
      return this.userService.create(createUserDto);
    }
  }

  // @Get()
  // findAll() {
  //   var data = this.userService.findAll();
  //   return data;
  // }

  @Get(':id')
  async findOne(@Param('id') id: string) {
    var user = await this.userService.findOneByUser(id);

    var users = await this.userService.findByToken(user.tokenNotification)
    const totalUser = users.length
    if (totalUser > 0) {
      return users
    } else {
      return user
    }
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateUserDto: UpdateUserDto) {
    return this.userService.update(+id, updateUserDto);
  }

  @Post('upload/:id')
  @UseInterceptors(FileInterceptor('file', {
    storage: diskStorage({
      destination: './files',
      filename: (req, file, cb) => {
        const fileNameSplit = file.originalname.split(".");
        const fileExt = fileNameSplit[fileNameSplit.length - 1];
        cb(null, `${Date.now()}.${fileExt}`);
      }
    })
  }))
  async uploadFile(@UploadedFile() file: Express.Multer.File, @Param('id') id: string) {
    let user = await this.userService.findOne(+id);


    if (user == null) {
      throw new HttpException('Not Find User in System', HttpStatus.FORBIDDEN);
    }

    var updateUserDto = new UpdateUserDto();
    updateUserDto.firstname = user.firstname;
    updateUserDto.studentId = user.studentId;
    updateUserDto.tokenNotification = user.tokenNotification;
    updateUserDto.profilePicture = "userimage/" + file.filename;
    // console.log(file);

    let newUser = await this.userService.update(+id, updateUserDto);

    return newUser;
  }

  // @Delete(':id')
  // remove(@Param('id') id: string) {
  //   return this.userService.remove(+id);
  // }
}
