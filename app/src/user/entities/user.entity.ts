import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity({ name: "users" })
export class User {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({ type: "varchar", length: 50 })
    firstname: string;

    @Column({ name: "student_id", type: "varchar", length: 50 })
    studentId: string;

    @Column({ name: "profile_picture", type: "text" })
    profilePicture: string;

    @Column({ name: "token_notification", type: "varchar", length: 50 })
    tokenNotification: string;

}
