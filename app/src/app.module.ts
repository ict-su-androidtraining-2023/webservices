import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserModule } from './user/user.module';
import { User } from './user/entities/user.entity';
import { TodoModule } from './todo/todo.module';
import { Todo } from './todo/entities/todo.entity';
import { ServeStaticModule } from '@nestjs/serve-static';
import { join } from 'path';

@Module({
  imports: [ServeStaticModule.forRoot({
    rootPath: join(__dirname, '..', 'files'),
    serveRoot: "/userimage",
  }), 
  ServeStaticModule.forRoot({
    rootPath: join(__dirname, '..', 'todofiles'),
    serveRoot: "/todoimage",
  }),
  TypeOrmModule.forRoot({
    type: 'postgres',
    host: 'db',
    port: 5432,
    username: 'postgres-query',
    password: 'vhfsmO@Iud4is26',
    database: 'Database-Training',
    entities: [User, Todo],
    synchronize: false,
  }),
    UserModule,
    TodoModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule { }
