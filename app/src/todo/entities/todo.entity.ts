import { User } from "src/user/entities/user.entity";
import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn } from "typeorm";

@Entity({ name: "todo" })
export class Todo { 
    @PrimaryGeneratedColumn()
    id: number;

    @Column({name:"user_id",type: "int"})
    userId: number;

    @Column({name:"header", type:"text"})
    header: string;

    @Column({name:"message_text", type:"text"})
    bodyMessage: string;

    @Column({name:"picture", type:"text"})
    picture: string;

    @Column({name:"location_name", type:"text"})
    locationName: string;

    @ManyToOne(() => User)
    @JoinColumn([
        { name: 'user_id', referencedColumnName: 'id'}
    ])
    user: User
}
