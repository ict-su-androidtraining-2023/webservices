import { Controller, Get, Post, Body, Patch, Param, Delete, Query, UseInterceptors, UploadedFile, HttpException, HttpStatus } from '@nestjs/common';
import { TodoService } from './todo.service';
import { CreateTodoDto } from './dto/create-todo.dto';
import { UpdateTodoDto } from './dto/update-todo.dto';
import { FileInterceptor } from '@nestjs/platform-express';
import { diskStorage } from 'multer';

@Controller('todo')
export class TodoController {
  constructor(private readonly todoService: TodoService) { }

  @Post()
  create(@Body() createTodoDto: CreateTodoDto) {
    return this.todoService.create(createTodoDto);
  }

  @Get()
  findAll(@Query('userid') userid: string) {
    return this.todoService.findAllByUser(userid);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.todoService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateTodoDto: UpdateTodoDto) {
    return this.todoService.update(+id, updateTodoDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.todoService.remove(+id);
  }

  @Post('upload/:id')
  @UseInterceptors(FileInterceptor('todofiles', {
    storage: diskStorage({
      destination: './todofiles',
      filename: (req, file, cb) => {
        const fileNameSplit = file.originalname.split(".");
        const fileExt = fileNameSplit[fileNameSplit.length - 1];
        cb(null, `${Date.now()}.${fileExt}`);
      }
    })
  }))
  async uploadFile(@UploadedFile() file: Express.Multer.File, @Param('id') id: string) {
    let todo = await this.todoService.findOne(+id);


    if (todo == null) {
      throw new HttpException('Not Find Todo in System', HttpStatus.FORBIDDEN);
    }

    var updateTodoDto = new UpdateTodoDto();
    updateTodoDto.header = todo.header;
    updateTodoDto.bodyMessage = todo.bodyMessage;
    updateTodoDto.userId = todo.userId;
    updateTodoDto.locationName = todo.locationName;
    updateTodoDto.picture = "todoimage/" + file.filename;
    console.log(file);
    let newTodo = await this.todoService.update(+id, updateTodoDto);
    // newTodo.picture = "todoimage/" + file.filename

    return newTodo
  }
}
