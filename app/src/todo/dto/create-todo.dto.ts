import { IsNotEmpty, IsNumber, IsString, MinLength } from "class-validator";

export class CreateTodoDto {

    @IsNumber()
    @IsNotEmpty()
    userId: number;

    @IsString()
    @MinLength(3, { message: 'ขอให้ตั้งหัวข้อมากกว่า 3 ตัวอักษร' })
    header: string;

    @IsString()
    @MinLength(5, { message: 'ขอให้ข้อความมากกว่า 5 ตัวอักษร' })
    bodyMessage: string;

    picture: string;

    locationName: string;
}
