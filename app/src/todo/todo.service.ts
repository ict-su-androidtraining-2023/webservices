import { Injectable } from '@nestjs/common';
import { CreateTodoDto } from './dto/create-todo.dto';
import { UpdateTodoDto } from './dto/update-todo.dto';
import { Todo } from './entities/todo.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class TodoService {

  constructor(
    @InjectRepository(Todo) private readonly todoRepo: Repository<Todo>,
    // @InjectRepository(User) private readonly userRepo: Repository<User>,
  ) { }

  async create(createTodoDto: CreateTodoDto) {
    const todo: Todo = new Todo()
    let userId = createTodoDto.userId;
    todo.userId = createTodoDto.userId;
    todo.header = createTodoDto.header;
    todo.bodyMessage = createTodoDto.bodyMessage;
    todo.picture = createTodoDto.picture;
    todo.locationName = createTodoDto.locationName;
    // todo.user = await this.userRepo.createQueryBuilder('users')
    // .where("users.id = :id", { id: userId })
    // .getOne();

    return this.todoRepo.save(todo);
  }

  findAllByUser(userid: string) {
    var result = this.todoRepo.createQueryBuilder('todo')
      .where("todo.user_id = :id", { id: userid })
      .getMany();

    return result;
  }

  findOne(id: number) {
    return this.todoRepo.findOneBy({ id });
  }

  update(id: number, updateTodoDto: UpdateTodoDto) {
    const todo: Todo = new Todo()
    todo.userId = updateTodoDto.userId;
    todo.header = updateTodoDto.header;
    todo.bodyMessage = updateTodoDto.bodyMessage;
    todo.picture = updateTodoDto.picture;
    todo.locationName = updateTodoDto.locationName;

    todo.id = id;

    return this.todoRepo.save(todo);
  }

  remove(id: number) {
    return this.todoRepo.delete(id);
  }
}
