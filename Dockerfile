# FROM node:18-alpine as installer

# WORKDIR /app

# CMD ["npm", "start"]

FROM node:18-alpine as runner

WORKDIR /app

RUN npm i -g @nestjs/cli
# RUN npm install

EXPOSE 3000
EXPOSE 5000

CMD ["npm", "start"]