# For Build Docker Image
- cd app
- docker build -t get-nest-service .

# Copy Docker Image to Server by SSH
- docker save get-nest-service:latest | ssh -C {user_server}@{Server Address} docker load

# Run Production
- Comment Section **dev**
- UnComment Section **services**
- docker compose up -d